//Initialisierung des Websocket-Servers
var http = require('http');
var WebSocketServer = require('websocket').server;

var server = http.createServer(function (request, response) {
    console.log('Anfrage empfangen');
    response.end();
});



//für die richtige Verbindung zum Server wird so etwas genutzt: ip= '85.215.237.239'
//server.listen(8092, ip, function Listening() ....

//Server lauscht dem Port 8090, ob es eine neue Client-Verbindung gibt
server.listen(8090, function Listening1() {
    console.log('Web-Server läuft und hört auf Port: 8090');

});

var WebSocketServer = require('websocket').server;

webSocketServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

//Die Anfragequlle wird geprüft
function iSOriginAllowed(origin) {
    return true;
}

webSocketServer.on('request', function (request) {

    if (!iSOriginAllowed(request.origin)) {
        request.reject();
        console.log('Verbindung von Client');
        return;
    }

    //Die Verbindung zum Client wird über das echo-protocol authorisiert
    var connection = request.accept("echo-protocol", request.origin);

    console.log('Client-Verbindung akzeptiert');

    //Hier werden die Clickstream-Daten vom Client empfangen und an das Topic Input-Stream geschickt
    km = new kafka.KeyedMessage('key', 'message'),

        connection.on('message', function (evt) {


            DT = evt.utf8Data
            payloads = [{ topic: 'Input-Stream', messages: DT, partition: 0 }];
            producer.send(payloads, function (err, payloads) {
                console.log('Clickstream-Daten empfangen und an den Input-Stream geschickt')


            });
        });

    //Mit dieser Methode prüft der Web-Server, ob es für die weiter unten definierten Kafka-Consumer neue Werte gibt, falls ja sendet er diese an den Client. Damit werden der Score und der Clickstream-Wert an den Client geschickt
    consumer.on('message', function (message) {
        connection.sendUTF(message.value);
    });

    connection.on('close', function (reasonCode, description) {
        console.log('Connection' + connection.remoteAddress + 'disconnected.');
    });
});


//Kafka-Verbindung wird definiert, Producer wird nur initialisiert, der Consumer wird auf die beiden Streams festgelegt
var ConsKaf = 0;
const { data } = require("jquery");

const kafka = require('kafka-node'),
    Producer = kafka.Producer,
    Consumer = kafka.Consumer,

    client = new kafka.KafkaClient("localhost:9092"),
    producer = new Producer(client);
consumer = new Consumer(client, [{ topic: 'Input-Stream', partition: 0 }, { topic: 'Result-Stream', partition: 0 }], { autoComit: false });

