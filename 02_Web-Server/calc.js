
//Der Websocket-Server wird initialisiert
const http = require('http');

const hostname = 'localhost';
const port = 3010;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
});

server.listen(port, hostname, () => {
  console.log('Analytic-Server läuft und hört auf Port: 3010 ');
});

var ConsKaf = 0;
const { data } = require("jquery");


//Kafka-Node wird benötigt, damit der Webserver mit dem Kafka-Server kommunizieren kann
const kafka = require('kafka-node'),

  Producer = kafka.Producer,
  Consumer = kafka.Consumer,

  //Der Web-Server prüft, ob auf dem localhost 9092 neue Werte bereitgestellt werde, auf diesem Port läuft der Kafka-Server
  client = new kafka.KafkaClient("localhost:9092"),

  //Der Kafka-Producer wird initialisiert
  producer = new Producer(client);

//Hier wird deklariert, dass der Kafka-Consumer auf den Input-Stream hören soll
consumer = new Consumer(client, [{ topic: 'Input-Stream', partition: 0 }], { autoComit: false });
km = new kafka.KeyedMessage('key', 'message')

//Ein Timer, der den Score langsam wieder senkt. Hier ist die Annahme, dass das Interesse nicht hoch genug ist, wenn über längere Zeit die Schwelle des Scores nicht erreicht wurde.
function DecreaseScore() {

  VarSend = 'Scoresenkung aller 30s'; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; producer.send(payloads, function (err, payloads) { });
  VarSend = -2; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; producer.send(payloads, function (err, payloads) { });
}

//Der Score wird dadurch aller 30 Sekunden um 2 Prozentpunkte gesenkt
setInterval(function () {

  DecreaseScore();

}, 30000);

//Alle Boolean-Werte die an Attribute des Webportals gekoppelt sind müssen initial 0 sein

function Variables_Initial() {
  c1URL = 0;
  Age_1 = 0;
  Age_2 = 0;
  Age_3 = 0;
  Age_4 = 0;
  Age_5 = 0;
  Age_6 = 0;
  Arb_1 = 0;
  Arb_2 = 0;
  Hom_1 = 0;
  Hom_2 = 0;
  Ans_1 = 0;
  Ans_2 = 0;
  Ans_3 = 0;
  Ans_4 = 0;
  Ans_5 = 0;
  Ans_6 = 0;
  Ans_7 = 0;
  Ort_1 = 0;
  Ort_2 = 0;
  Ort_3 = 0;
  Spr_1 = 0;
  Spr_2 = 0;
  Spr_3 = 0;
  Spr_4 = 0;
  Spr_5 = 0;
  Spr_6 = 0;
  Spr_7 = 0;
  Ber_1 = 0;
  Ber_2 = 0;
  Ber_3 = 0;
  Geh_1 = 0;
  Geh_2 = 0;
  Geh_3 = 0;
  C1_URL = 0;
  C1_Like = 0;
  C1_Details_1 = 0;
  C1_Details_2 = 0;
  C1_Details_3 = 0;
  C1_Bewerbung = 0;
  C2_URL = 0;
  C2_Like = 0;
  C2_Details_1 = 0;
  C2_Details_2 = 0;
  C2_Details_3 = 0;
  C2_Bewerbung = 0;
  C3_URL = 0;
  C3_Like = 0;
  C3_Details_1 = 0;
  C3_Details_2 = 0;
  C3_Details_3 = 0;
  C3_Bewerbung = 0;
  C4_URL = 0;
  C4_Like = 0;
  C4_Details_1 = 0;
  C4_Details_2 = 0;
  C4_Details_3 = 0;
  C4_Bewerbung = 0;
  C5_URL = 0;
  C5_Like = 0;
  C5_Details_1 = 0;
  C5_Details_2 = 0;
  C5_Details_3 = 0;
  C5_Bewerbung = 0;
  C6_URL = 0;
  C6_Like = 0;
  C6_Details_1 = 0;
  C6_Details_2 = 0;
  C6_Details_3 = 0;
  C6_Bewerbung = 0;
  C7_URL = 0;
  C7_Like = 0;
  C7_Details_1 = 0;
  C7_Details_2 = 0;
  C7_Bewerbung = 0;
  C8_URL = 0;
  C8_Like = 0;
  C8_Details_1 = 0;
  C8_Details_2 = 0;
  C8_Details_3 = 0;
  C8_Bewerbung = 0;
  C9_URL = 0;
  C9_Like = 0;
  C9_Details_1 = 0;
  C9_Details_2 = 0;
  C9_Details_3 = 0;
  C9_Bewerbung = 0;
  C10_URL = 0;
  C10_Like = 0;
  C10_Details_1 = 0;
  C10_Details_2 = 0;
  C10_Details_3 = 0;
  C10_Bewerbung = 0;
  C11_URL = 0;
  C11_Like = 0;
  C11_Details_1 = 0;
  C11_Details_2 = 0;
  C11_Details_3 = 0;
  C11_Bewerbung = 0;
  C12_URL = 0;
  C12_Like = 0;
  C12_Details_1 = 0;
  C12_Details_2 = 0;
  C12_Details_3 = 0;
  C12_Bewerbung = 0;
  C13_URL = 0;
  C13_Like = 0;
  C13_Details_1 = 0;
  C13_Details_2 = 0;
  C13_Details_3 = 0;
  C13_Bewerbung = 0;
  C14_URL = 0;
  C14_Like = 0;
  C14_Details_1 = 0;
  C14_Details_2 = 0;
  C14_Details_3 = 0;
  C14_Bewerbung = 0;
  C15_URL = 0;
  C15_Like = 0;
  C15_Details_1 = 0;
  C15_Details_2 = 0;
  C15_Details_3 = 0;
  C15_Bewerbung = 0;
  C16_URL = 0;
  C16_Like = 0;
  C16_Details_1 = 0;
  C16_Details_2 = 0;
  C16_Details_3 = 0;
  C16_Bewerbung = 0;
  C17_URL = 0;
  C17_Like = 0;
  C17_Details_1 = 0;
  C17_Details_2 = 0;
  C17_Details_3 = 0;
  C17_Bewerbung = 0;
  C18_URL = 0;
  C18_Like = 0;
  C18_Details_1 = 0;
  C18_Details_2 = 0;
  C18_Details_3 = 0;
  C18_Bewerbung = 0;
  C19_URL = 0;
  C19_Like = 0;
  C19_Details_1 = 0;
  C19_Details_2 = 0;
  C19_Details_3 = 0;
  C19_Bewerbung = 0;
  C20_URL = 0;
  C20_Like = 0;
  C20_Details_1 = 0;
  C20_Details_2 = 0;
  C20_Details_3 = 0;
  C20_Bewerbung = 0;
};
Variables_Initial();

//Die Consumer-Methode führt alle Befehle aus, falls ein neuer Wert über im Topic ankommt
consumer.on("message", function (message) {
  console.log("Neue Daten vom Topic Input-Stream angekommen!")

  //Hier wird geprüft, ob der Reset-Button gedrückt wurde, falls ja werden alle Boolean-Werte in die Ausgangsposition gesetzt.
  if (message.value == "reset") {

    c1URL = 0;
    Age_1 = 0;
    Age_2 = 0;
    Age_3 = 0;
    Age_4 = 0;
    Age_5 = 0;
    Age_6 = 0;
    Arb_1 = 0;
    Arb_2 = 0;
    Hom_1 = 0;
    Hom_2 = 0;
    Ans_1 = 0;
    Ans_2 = 0;
    Ans_3 = 0;
    Ans_4 = 0;
    Ans_5 = 0;
    Ans_6 = 0;
    Ans_7 = 0;
    Ort_1 = 0;
    Ort_2 = 0;
    Ort_3 = 0;
    Spr_1 = 0;
    Spr_2 = 0;
    Spr_3 = 0;
    Spr_4 = 0;
    Spr_5 = 0;
    Spr_6 = 0;
    Spr_7 = 0;
    Ber_1 = 0;
    Ber_2 = 0;
    Ber_3 = 0;
    Geh_1 = 0;
    Geh_2 = 0;
    Geh_3 = 0;
    C1_URL = 0;
    C1_Like = 0;
    C1_Details_1 = 0;
    C1_Details_2 = 0;
    C1_Details_3 = 0;
    C1_Bewerbung = 0;
    C2_URL = 0;
    C2_Like = 0;
    C2_Details_1 = 0;
    C2_Details_2 = 0;
    C2_Details_3 = 0;
    C2_Bewerbung = 0;
    C3_URL = 0;
    C3_Like = 0;
    C3_Details_1 = 0;
    C3_Details_2 = 0;
    C3_Details_3 = 0;
    C3_Bewerbung = 0;
    C4_URL = 0;
    C4_Like = 0;
    C4_Details_1 = 0;
    C4_Details_2 = 0;
    C4_Details_3 = 0;
    C4_Bewerbung = 0;
    C5_URL = 0;
    C5_Like = 0;
    C5_Details_1 = 0;
    C5_Details_2 = 0;
    C5_Details_3 = 0;
    C5_Bewerbung = 0;
    C6_URL = 0;
    C6_Like = 0;
    C6_Details_1 = 0;
    C6_Details_2 = 0;
    C6_Details_3 = 0;
    C6_Bewerbung = 0;
    C7_URL = 0;
    C7_Like = 0;
    C7_Details_1 = 0;
    C7_Details_2 = 0;
    C7_Bewerbung = 0;
    C8_URL = 0;
    C8_Like = 0;
    C8_Details_1 = 0;
    C8_Details_2 = 0;
    C8_Details_3 = 0;
    C8_Bewerbung = 0;
    C9_URL = 0;
    C9_Like = 0;
    C9_Details_1 = 0;
    C9_Details_2 = 0;
    C9_Details_3 = 0;
    C9_Bewerbung = 0;
    C10_URL = 0;
    C10_Like = 0;
    C10_Details_1 = 0;
    C10_Details_2 = 0;
    C10_Details_3 = 0;
    C10_Bewerbung = 0;
    C11_URL = 0;
    C11_Like = 0;
    C11_Details_1 = 0;
    C11_Details_2 = 0;
    C11_Details_3 = 0;
    C11_Bewerbung = 0;
    C12_URL = 0;
    C12_Like = 0;
    C12_Details_1 = 0;
    C12_Details_2 = 0;
    C12_Details_3 = 0;
    C12_Bewerbung = 0;
    C13_URL = 0;
    C13_Like = 0;
    C13_Details_1 = 0;
    C13_Details_2 = 0;
    C13_Details_3 = 0;
    C13_Bewerbung = 0;
    C14_URL = 0;
    C14_Like = 0;
    C14_Details_1 = 0;
    C14_Details_2 = 0;
    C14_Details_3 = 0;
    C14_Bewerbung = 0;
    C15_URL = 0;
    C15_Like = 0;
    C15_Details_1 = 0;
    C15_Details_2 = 0;
    C15_Details_3 = 0;
    C15_Bewerbung = 0;
    C16_URL = 0;
    C16_Like = 0;
    C16_Details_1 = 0;
    C16_Details_2 = 0;
    C16_Details_3 = 0;
    C16_Bewerbung = 0;
    C17_URL = 0;
    C17_Like = 0;
    C17_Details_1 = 0;
    C17_Details_2 = 0;
    C17_Details_3 = 0;
    C17_Bewerbung = 0;
    C18_URL = 0;
    C18_Like = 0;
    C18_Details_1 = 0;
    C18_Details_2 = 0;
    C18_Details_3 = 0;
    C18_Bewerbung = 0;
    C19_URL = 0;
    C19_Like = 0;
    C19_Details_1 = 0;
    C19_Details_2 = 0;
    C19_Details_3 = 0;
    C19_Bewerbung = 0;
    C20_URL = 0;
    C20_Like = 0;
    C20_Details_1 = 0;
    C20_Details_2 = 0;
    C20_Details_3 = 0;
    C20_Bewerbung = 0;

  }

//Es wird geprüft, ob die Clickstream-Daten aus dem Topic Input-Stream mit einer der Variablen übereinstimmen, falls ja und falls der Click darauf noch nocht gemacht wurde, dann wird mit VarSend ein Score-Wert definiert,
// der anschließend in den Result-Stream geschickt wird. Anschließend wird die Varaible auf 1 gesetzt und kann bis zum Reset den Score nicht mehr beeinflussen.
  
  if (message.value == "Age_1") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Age_1 != 1) { producer.send(payloads, function (err, payloads) { }); Age_1 = 1; } };
  if (message.value == "Age_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Age_2 != 1) { producer.send(payloads, function (err, payloads) { }); Age_2 = 1; } };
  if (message.value == "Age_3") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Age_3 != 1) { producer.send(payloads, function (err, payloads) { }); Age_3 = 1; } };
  if (message.value == "Age_4") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Age_4 != 1) { producer.send(payloads, function (err, payloads) { }); Age_4 = 1; } };
  if (message.value == "Age_5") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Age_5 != 1) { producer.send(payloads, function (err, payloads) { }); Age_5 = 1; } };
  if (message.value == "Age_6") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Age_6 != 1) { producer.send(payloads, function (err, payloads) { }); Age_6 = 1; } };

  if (message.value == "Arb_1") { VarSend = 5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Arb_1 != 1) { producer.send(payloads, function (err, payloads) { }); Arb_1 = 1; } };
  if (message.value == "Arb_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Arb_2 != 1) { producer.send(payloads, function (err, payloads) { }); Arb_2 = 1; } };

  if (message.value == "Hom_1") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Hom_1 != 1) { producer.send(payloads, function (err, payloads) { }); Hom_1 = 1; } };
  if (message.value == "Hom_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Hom_2 != 1) { producer.send(payloads, function (err, payloads) { }); Hom_2 = 1; } };

  if (message.value == "Ans_1") { VarSend = 15; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ans_1 != 1) { producer.send(payloads, function (err, payloads) { }); Ans_1 = 1; } };
  if (message.value == "Ans_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ans_2 != 1) { producer.send(payloads, function (err, payloads) { }); Ans_2 = 1; } };
  if (message.value == "Ans_3") { VarSend = -20; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ans_3 != 1) { producer.send(payloads, function (err, payloads) { }); Ans_3 = 1; } };
  if (message.value == "Ans_4") { VarSend = -30; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ans_4 != 1) { producer.send(payloads, function (err, payloads) { }); Ans_4 = 1; } };
  if (message.value == "Ans_5") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ans_5 != 1) { producer.send(payloads, function (err, payloads) { }); Ans_5 = 1; } };
  if (message.value == "Ans_6") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ans_6 != 1) { producer.send(payloads, function (err, payloads) { }); Ans_6 = 1; } };
  if (message.value == "Ans_7") { VarSend = -30; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ans_7 != 1) { producer.send(payloads, function (err, payloads) { }); Ans_7 = 1; } };

  if (message.value == "Ort_1") { VarSend = 20; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ort_1 != 1) { producer.send(payloads, function (err, payloads) { }); Ort_1 = 1; } };
  if (message.value == "Ort_2") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ort_2 != 1) { producer.send(payloads, function (err, payloads) { }); Ort_2 = 1; } };
  if (message.value == "Ort_3") { VarSend = -10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ort_3 != 1) { producer.send(payloads, function (err, payloads) { }); Ort_3 = 1; } };

  if (message.value == "Spr_1") { VarSend = 30; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Spr_1 != 1) { producer.send(payloads, function (err, payloads) { }); Spr_1 = 1; } };
  if (message.value == "Spr_2") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Spr_2 != 1) { producer.send(payloads, function (err, payloads) { }); Spr_2 = 1; } };
  if (message.value == "Spr_3") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Spr_3 != 1) { producer.send(payloads, function (err, payloads) { }); Spr_3 = 1; } };
  if (message.value == "Spr_4") { VarSend = 5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Spr_4 != 1) { producer.send(payloads, function (err, payloads) { }); Spr_4 = 1; } };
  if (message.value == "Spr_5") { VarSend = 5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Spr_5 != 1) { producer.send(payloads, function (err, payloads) { }); Spr_5 = 1; } };
  if (message.value == "Spr_6") { VarSend = -10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Spr_6 != 1) { producer.send(payloads, function (err, payloads) { }); Spr_6 = 1; } };
  if (message.value == "Spr_7") { VarSend = -20; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Spr_7 != 1) { producer.send(payloads, function (err, payloads) { }); Spr_7 = 1; } };

  if (message.value == "Ber_1") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ber_1 != 1) { producer.send(payloads, function (err, payloads) { }); Ber_1 = 1; } };
  if (message.value == "Ber_2") { VarSend = -10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ber_2 != 1) { producer.send(payloads, function (err, payloads) { }); Ber_2 = 1; } };
  if (message.value == "Ber_3") { VarSend = 20; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Ber_3 != 1) { producer.send(payloads, function (err, payloads) { }); Ber_3 = 1; } };

  if (message.value == "Geh_1") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Geh_1 != 1) { producer.send(payloads, function (err, payloads) { }); Geh_1 = 1; } };
  if (message.value == "Geh_2") { VarSend = 20; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Geh_2 != 1) { producer.send(payloads, function (err, payloads) { }); Geh_2 = 1; } };
  if (message.value == "Geh_3") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (Geh_3 != 1) { producer.send(payloads, function (err, payloads) { }); Geh_3 = 1; } };

  if (message.value == "C1_URL") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C1_URL != 1) { producer.send(payloads, function (err, payloads) { }); C1_URL = 1; } };
  if (message.value == "C1_Like") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C1_Like != 1) { producer.send(payloads, function (err, payloads) { }); C1_Like = 1; } };
  if (message.value == "C1_Details_1") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C1_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C1_Details_1 = 1; } };
  if (message.value == "C1_Details_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C1_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C1_Details_2 = 1; } };
  if (message.value == "C1_Details_3") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C1_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C1_Details_3 = 1; } };
  if (message.value == "C1_Bewerbung") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C1_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C1_Bewerbung = 1; } };

  if (message.value == "C2_URL") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C2_URL != 1) { producer.send(payloads, function (err, payloads) { }); C2_URL = 1; } };
  if (message.value == "C2_Like") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C2_Like != 1) { producer.send(payloads, function (err, payloads) { }); C2_Like = 1; } };
  if (message.value == "C2_Details_1") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C2_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C2_Details_1 = 1; } };
  if (message.value == "C2_Details_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C2_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C2_Details_2 = 1; } };
  if (message.value == "C2_Details_3") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C2_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C2_Details_3 = 1; } };
  if (message.value == "C2_Bewerbung") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C2_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C2_Bewerbung = 1; } };

  if (message.value == "C3_URL") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C3_URL != 1) { producer.send(payloads, function (err, payloads) { }); C3_URL = 1; } };
  if (message.value == "C3_Like") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C3_Like != 1) { producer.send(payloads, function (err, payloads) { }); C3_Like = 1; } };
  if (message.value == "C3_Details_1") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C3_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C3_Details_1 = 1; } };
  if (message.value == "C3_Details_2") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C3_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C3_Details_2 = 1; } };
  if (message.value == "C3_Details_3") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C3_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C3_Details_3 = 1; } };
  if (message.value == "C3_Bewerbung") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C3_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C3_Bewerbung = 1; } };

  if (message.value == "C4_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C4_URL != 1) { producer.send(payloads, function (err, payloads) { }); C4_URL = 1; } };
  if (message.value == "C4_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C4_Like != 1) { producer.send(payloads, function (err, payloads) { }); C4_Like = 1; } };
  if (message.value == "C4_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C4_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C4_Details_1 = 1; } };
  if (message.value == "C4_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C4_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C4_Details_2 = 1; } };
  if (message.value == "C4_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C4_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C4_Details_3 = 1; } };
  if (message.value == "C4_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C4_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C4_Bewerbung = 1; } };

  if (message.value == "C5_URL") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C5_URL != 1) { producer.send(payloads, function (err, payloads) { }); C5_URL = 1; } };
  if (message.value == "C5_Like") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C5_Like != 1) { producer.send(payloads, function (err, payloads) { }); C5_Like = 1; } };
  if (message.value == "C5_Details_1") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C5_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C5_Details_1 = 1; } };
  if (message.value == "C5_Details_2") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C5_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C5_Details_2 = 1; } };
  if (message.value == "C5_Details_3") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C5_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C5_Details_3 = 1; } };
  if (message.value == "C5_Bewerbung") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C5_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C5_Bewerbung = 1; } };

  if (message.value == "C6_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C6_URL != 1) { producer.send(payloads, function (err, payloads) { }); C6_URL = 1; } };
  if (message.value == "C6_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C6_Like != 1) { producer.send(payloads, function (err, payloads) { }); C6_Like = 1; } };
  if (message.value == "C6_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C6_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C6_Details_1 = 1; } };
  if (message.value == "C6_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C6_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C6_Details_2 = 1; } };
  if (message.value == "C6_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C6_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C6_Details_3 = 1; } };
  if (message.value == "C6_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C6_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C6_Bewerbung = 1; } };

  if (message.value == "C7_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C7_URL != 1) { producer.send(payloads, function (err, payloads) { }); C7_URL = 1; } };
  if (message.value == "C7_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C7_Like != 1) { producer.send(payloads, function (err, payloads) { }); C7_Like = 1; } };
  if (message.value == "C7_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C7_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C7_Details_1 = 1; } };
  if (message.value == "C7_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C7_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C7_Details_2 = 1; } };
  if (message.value == "C7_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C7_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C7_Details_3 = 1; } };
  if (message.value == "C7_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C7_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C7_Bewerbung = 1; } };

  if (message.value == "C8_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C8_URL != 1) { producer.send(payloads, function (err, payloads) { }); C8_URL = 1; } };
  if (message.value == "C8_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C8_Like != 1) { producer.send(payloads, function (err, payloads) { }); C8_Like = 1; } };
  if (message.value == "C8_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C8_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C8_Details_1 = 1; } };
  if (message.value == "C8_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C8_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C8_Details_2 = 1; } };
  if (message.value == "C8_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C8_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C8_Details_3 = 1; } };
  if (message.value == "C8_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C8_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C8_Bewerbung = 1; } };

  if (message.value == "C9_URL") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C9_URL != 1) { producer.send(payloads, function (err, payloads) { }); C9_URL = 1; } };
  if (message.value == "C9_Like") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C9_Like != 1) { producer.send(payloads, function (err, payloads) { }); C9_Like = 1; } };
  if (message.value == "C9_Details_1") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C9_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C9_Details_1 = 1; } };
  if (message.value == "C9_Details_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C9_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C9_Details_2 = 1; } };
  if (message.value == "C9_Details_3") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C9_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C9_Details_3 = 1; } };
  if (message.value == "C9_Bewerbung") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C9_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C9_Bewerbung = 1; } };

  if (message.value == "C10_URL") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C10_URL != 1) { producer.send(payloads, function (err, payloads) { }); C10_URL = 1; } };
  if (message.value == "C10_Like") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C10_Like != 1) { producer.send(payloads, function (err, payloads) { }); C10_Like = 1; } };
  if (message.value == "C10_Details_1") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C10_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C10_Details_1 = 1; } };
  if (message.value == "C10_Details_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C10_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C10_Details_2 = 1; } };
  if (message.value == "C10_Details_3") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C10_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C10_Details_3 = 1; } };
  if (message.value == "C10_Bewerbung") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C10_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C10_Bewerbung = 1; } };

  if (message.value == "C11_URL") { VarSend = 15; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C11_URL != 1) { producer.send(payloads, function (err, payloads) { }); C11_URL = 1; } };
  if (message.value == "C11_Like") { VarSend = 15; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C11_Like != 1) { producer.send(payloads, function (err, payloads) { }); C11_Like = 1; } };
  if (message.value == "C11_Details_1") { VarSend = 15; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C11_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C11_Details_1 = 1; } };
  if (message.value == "C11_Details_2") { VarSend = 15; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C11_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C11_Details_2 = 1; } };
  if (message.value == "C11_Details_3") { VarSend = 15; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C11_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C11_Details_3 = 1; } };
  if (message.value == "C11_Bewerbung") { VarSend = 15; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C11_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C11_Bewerbung = 1; } };

  if (message.value == "C12_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C12_URL != 1) { producer.send(payloads, function (err, payloads) { }); C12_URL = 1; } };
  if (message.value == "C12_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C12_Like != 1) { producer.send(payloads, function (err, payloads) { }); C12_Like = 1; } };
  if (message.value == "C12_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C12_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C12_Details_1 = 1; } };
  if (message.value == "C12_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C12_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C12_Details_2 = 1; } };
  if (message.value == "C12_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C12_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C12_Details_3 = 1; } };
  if (message.value == "C12_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C12_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C12_Bewerbung = 1; } };

  if (message.value == "C13_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C13_URL != 1) { producer.send(payloads, function (err, payloads) { }); C13_URL = 1; } };
  if (message.value == "C13_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C13_Like != 1) { producer.send(payloads, function (err, payloads) { }); C13_Like = 1; } };
  if (message.value == "C13_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C13_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C13_Details_1 = 1; } };
  if (message.value == "C13_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C13_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C13_Details_2 = 1; } };
  if (message.value == "C13_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C13_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C13_Details_3 = 1; } };
  if (message.value == "C13_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C13_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C13_Bewerbung = 1; } };

  if (message.value == "C14_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C14_URL != 1) { producer.send(payloads, function (err, payloads) { }); C14_URL = 1; } };
  if (message.value == "C14_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C14_Like != 1) { producer.send(payloads, function (err, payloads) { }); C14_Like = 1; } };
  if (message.value == "C14_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C14_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C14_Details_1 = 1; } };
  if (message.value == "C14_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C14_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C14_Details_2 = 1; } };
  if (message.value == "C14_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C14_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C14_Details_3 = 1; } };
  if (message.value == "C14_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C14_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C14_Bewerbung = 1; } };

  if (message.value == "C15_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C15_URL != 1) { producer.send(payloads, function (err, payloads) { }); C15_URL = 1; } };
  if (message.value == "C15_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C15_Like != 1) { producer.send(payloads, function (err, payloads) { }); C15_Like = 1; } };
  if (message.value == "C15_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C15_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C15_Details_1 = 1; } };
  if (message.value == "C15_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C15_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C15_Details_2 = 1; } };
  if (message.value == "C15_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C15_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C15_Details_3 = 1; } };
  if (message.value == "C15_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C15_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C15_Bewerbung = 1; } };

  if (message.value == "C16_URL") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C16_URL != 1) { producer.send(payloads, function (err, payloads) { }); C16_URL = 1; } };
  if (message.value == "C16_Like") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C16_Like != 1) { producer.send(payloads, function (err, payloads) { }); C16_Like = 1; } };
  if (message.value == "C16_Details_1") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C16_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C16_Details_1 = 1; } };
  if (message.value == "C16_Details_2") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C16_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C16_Details_2 = 1; } };
  if (message.value == "C16_Details_3") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C16_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C16_Details_3 = 1; } };
  if (message.value == "C16_Bewerbung") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C16_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C16_Bewerbung = 1; } };

  if (message.value == "C17_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C17_URL != 1) { producer.send(payloads, function (err, payloads) { }); C17_URL = 1; } };
  if (message.value == "C17_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C17_Like != 1) { producer.send(payloads, function (err, payloads) { }); C17_Like = 1; } };
  if (message.value == "C17_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C17_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C17_Details_1 = 1; } };
  if (message.value == "C17_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C17_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C17_Details_2 = 1; } };
  if (message.value == "C17_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C17_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C17_Details_3 = 1; } };
  if (message.value == "C17_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C17_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C17_Bewerbung = 1; } };

  if (message.value == "C18_URL") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C18_URL != 1) { producer.send(payloads, function (err, payloads) { }); C18_URL = 1; } };
  if (message.value == "C18_Like") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C18_Like != 1) { producer.send(payloads, function (err, payloads) { }); C18_Like = 1; } };
  if (message.value == "C18_Details_1") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C18_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C18_Details_1 = 1; } };
  if (message.value == "C18_Details_2") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C18_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C18_Details_2 = 1; } };
  if (message.value == "C18_Details_3") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C18_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C18_Details_3 = 1; } };
  if (message.value == "C18_Bewerbung") { VarSend = 10; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C18_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C18_Bewerbung = 1; } };

  if (message.value == "C19_URL") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C19_URL != 1) { producer.send(payloads, function (err, payloads) { }); C19_URL = 1; } };
  if (message.value == "C19_Like") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C19_Like != 1) { producer.send(payloads, function (err, payloads) { }); C19_Like = 1; } };
  if (message.value == "C19_Details_1") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C19_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C19_Details_1 = 1; } };
  if (message.value == "C19_Details_2") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C19_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C19_Details_2 = 1; } };
  if (message.value == "C19_Details_3") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C19_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C19_Details_3 = 1; } };
  if (message.value == "C19_Bewerbung") { VarSend = 1; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C19_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C19_Bewerbung = 1; } };

  if (message.value == "C20_URL") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C20_URL != 1) { producer.send(payloads, function (err, payloads) { }); C20_URL = 1; } };
  if (message.value == "C20_Like") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C20_Like != 1) { producer.send(payloads, function (err, payloads) { }); C20_Like = 1; } };
  if (message.value == "C20_Details_1") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C20_Details_1 != 1) { producer.send(payloads, function (err, payloads) { }); C20_Details_1 = 1; } };
  if (message.value == "C20_Details_2") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C20_Details_2 != 1) { producer.send(payloads, function (err, payloads) { }); C20_Details_2 = 1; } };
  if (message.value == "C20_Details_3") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C20_Details_3 != 1) { producer.send(payloads, function (err, payloads) { }); C20_Details_3 = 1; } };
  if (message.value == "C20_Bewerbung") { VarSend = -5; payloads = [{ topic: 'Result-Stream', messages: VarSend, partition: 0 }]; if (C20_Bewerbung != 1) { producer.send(payloads, function (err, payloads) { }); C20_Bewerbung = 1; } };
})