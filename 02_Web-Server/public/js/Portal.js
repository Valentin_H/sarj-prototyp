var Stream_Var = "Log-Start:"
var Score = 0
var Score_Sum = 0

// Aufbau und Verbindung zu localhost:8090

function webSocketInvoke() {

  var ws2 = new WebSocket("ws://localhost:8090/", "echo-protocol")

  //für die richtige Verbindung zum Server wird so etwas genutzt: var ws2 = new WebSocket("ws://85.215.237.239:8092", "echo-protocol")


  ws2.onopen = function () {
    console.log('Verbindung zum Web-Server hergestellt');
    function Initial_Reset () { ws2.send('reset') }
    Initial_Reset()
    ws2.onmessage = function (evt) {
      
      Score_Value = evt.data

      
      //Converts string in int, es wird hier der Result-Stream analysiert, der neben dem Score-Wert auch den Clickstream-Wert des Elements enthält, dies ist natürlich nur zu Testzwecken sinnvoll, sonst kann auch nur der Score geschickt werden
      Score_Value_Integer = parseInt(Score_Value)

      //Prüft, ob der Score ein Integer ist und schickt dann ein Score-Update
      if (Number.isInteger(Score_Value_Integer)) {
        UpdateScore()
      }


      //Score wird aktualisiert und die neue Summe wird gebildet
      function UpdateScore() {
        Score = +Score + +Score_Value_Integer
        Score_Sum = Score

        //Prüft, ob der Score unter 0 fallen würde, setzt den Score immer auf mindestens 0
        if (Score_Sum > -1 && Score_Sum < 101) {
          document.getElementById("Score_Value").textContent = Score_Sum
        }
        //Prüft, ob der Score auf über 100 kommen würde, setzt den Score immer auf höchstens 100
        else if (Score_Sum > 100) {
          Score_Full = 100
          Score = 100
          document.getElementById("Score_Value").textContent = Score_Full

        }
        //Der Score wird zurückgesetzt
        else if (Score_Sum < 0) {
          Score_Zero = 0
          Score = 0
          document.getElementById("Score_Value").textContent = Score_Zero
        }
      }

      Stream_Var = Score_Value + '<br>' + Stream_Var
      Stream_New = Stream_Var
      document.getElementById("StreamContent").innerHTML = '<p>' + Stream_New + ' </p>'

      //Hier ist der Score-Wert definiert, bei dem das Ereignis des Pop-Up ausgelöst wird
      if (Score_Sum >= 80) {
        confirm('Guten Tag, durch Ihre Suche auf dem Jobportal haben wir bemerkt, dass Sie sehr gut für die Stelle als Senior Java-Entwickler des Unternehmens Gerifficon Software Solution GmbH geeignet sind (Job-Anzeige Nr. 11). Der Leiter der Softwareentwicklung Herr Müller würde gerne jetzt mit Ihnen per Telefon kurz über die Rahmenebdingungen diese Stelle sprechen. Sie können Ihn bis 18:00 Uhr unter: 0341 123 456 789 erreichen. - Ihr Job-Portal-Team')
      }

    }

    //Clickevent für den Reset des Score-Values, Update Boolean-Werte auf dem Analytik-Server
    document.getElementById("reset").addEventListener("click", function () { ws2.send('reset') })


    //Erfasst alle Clickstream-Daten und sendet es an den Web-Server, der es in den Input-Stream schickt

    document.getElementById("Age_1").addEventListener("click", function () { ws2.send('Age_1') })
    document.getElementById("Age_2").addEventListener("click", function () { ws2.send('Age_2') })
    document.getElementById("Age_3").addEventListener("click", function () { ws2.send('Age_3') })
    document.getElementById("Age_4").addEventListener("click", function () { ws2.send('Age_4') })
    document.getElementById("Age_5").addEventListener("click", function () { ws2.send('Age_5') })
    document.getElementById("Age_6").addEventListener("click", function () { ws2.send('Age_6') })

    document.getElementById("Arb_1").addEventListener("click", function () { ws2.send('Arb_1') })
    document.getElementById("Arb_2").addEventListener("click", function () { ws2.send('Arb_2') })

    document.getElementById("Hom_1").addEventListener("click", function () { ws2.send('Hom_1') })
    document.getElementById("Hom_2").addEventListener("click", function () { ws2.send('Hom_2') })

    document.getElementById("Ans_1").addEventListener("click", function () { ws2.send('Ans_1') })
    document.getElementById("Ans_2").addEventListener("click", function () { ws2.send('Ans_2') })
    document.getElementById("Ans_3").addEventListener("click", function () { ws2.send('Ans_3') })
    document.getElementById("Ans_4").addEventListener("click", function () { ws2.send('Ans_4') })
    document.getElementById("Ans_5").addEventListener("click", function () { ws2.send('Ans_5') })
    document.getElementById("Ans_6").addEventListener("click", function () { ws2.send('Ans_6') })
    document.getElementById("Ans_7").addEventListener("click", function () { ws2.send('Ans_7') })

    document.getElementById("Ort_1").addEventListener("click", function () { ws2.send('Ort_1') })
    document.getElementById("Ort_2").addEventListener("click", function () { ws2.send('Ort_2') })
    document.getElementById("Ort_3").addEventListener("click", function () { ws2.send('Ort_3') })

    document.getElementById("Spr_1").addEventListener("click", function () { ws2.send('Spr_1') })
    document.getElementById("Spr_2").addEventListener("click", function () { ws2.send('Spr_2') })
    document.getElementById("Spr_3").addEventListener("click", function () { ws2.send('Spr_3') })
    document.getElementById("Spr_4").addEventListener("click", function () { ws2.send('Spr_4') })
    document.getElementById("Spr_5").addEventListener("click", function () { ws2.send('Spr_5') })
    document.getElementById("Spr_6").addEventListener("click", function () { ws2.send('Spr_6') })
    document.getElementById("Spr_7").addEventListener("click", function () { ws2.send('Spr_7') })

    document.getElementById("Ber_1").addEventListener("click", function () { ws2.send('Ber_1') })
    document.getElementById("Ber_2").addEventListener("click", function () { ws2.send('Ber_2') })
    document.getElementById("Ber_3").addEventListener("click", function () { ws2.send('Ber_3') })

    document.getElementById("Geh_1").addEventListener("click", function () { ws2.send('Geh_1') })
    document.getElementById("Geh_2").addEventListener("click", function () { ws2.send('Geh_2') })
    document.getElementById("Geh_3").addEventListener("click", function () { ws2.send('Geh_3') })

    document.getElementById("C1_URL").addEventListener("click", function () { ws2.send('C1_URL') })
    document.getElementById("C1_Like").addEventListener("click", function () { ws2.send('C1_Like') })
    document.getElementById("C1_Details_1").addEventListener("click", function () { ws2.send('C1_Details_1') })
    document.getElementById("C1_Details_2").addEventListener("click", function () { ws2.send('C1_Details_2') })
    document.getElementById("C1_Details_3").addEventListener("click", function () { ws2.send('C1_Details_3') })
    document.getElementById("C1_Bewerbung").addEventListener("click", function () { ws2.send('C1_Bewerbung') })

    document.getElementById("C2_URL").addEventListener("click", function () { ws2.send('C2_URL') })
    document.getElementById("C2_Like").addEventListener("click", function () { ws2.send('C2_Like') })
    document.getElementById("C2_Details_1").addEventListener("click", function () { ws2.send('C2_Details_1') })
    document.getElementById("C2_Details_2").addEventListener("click", function () { ws2.send('C2_Details_2') })
    document.getElementById("C2_Details_3").addEventListener("click", function () { ws2.send('C2_Details_3') })
    document.getElementById("C2_Bewerbung").addEventListener("click", function () { ws2.send('C2_Bewerbung') })

    document.getElementById("C3_URL").addEventListener("click", function () { ws2.send('C3_URL') })
    document.getElementById("C3_Like").addEventListener("click", function () { ws2.send('C3_Like') })
    document.getElementById("C3_Details_1").addEventListener("click", function () { ws2.send('C3_Details_1') })
    document.getElementById("C3_Details_2").addEventListener("click", function () { ws2.send('C3_Details_2') })
    document.getElementById("C3_Details_3").addEventListener("click", function () { ws2.send('C3_Details_3') })
    document.getElementById("C3_Bewerbung").addEventListener("click", function () { ws2.send('C3_Bewerbung') })

    document.getElementById("C4_URL").addEventListener("click", function () { ws2.send('C4_URL') })
    document.getElementById("C4_Like").addEventListener("click", function () { ws2.send('C4_Like') })
    document.getElementById("C4_Details_1").addEventListener("click", function () { ws2.send('C4_Details_1') })
    document.getElementById("C4_Details_2").addEventListener("click", function () { ws2.send('C4_Details_2') })
    document.getElementById("C4_Details_3").addEventListener("click", function () { ws2.send('C4_Details_3') })
    document.getElementById("C4_Bewerbung").addEventListener("click", function () { ws2.send('C4_Bewerbung') })

    document.getElementById("C5_URL").addEventListener("click", function () { ws2.send('C5_URL') })
    document.getElementById("C5_Like").addEventListener("click", function () { ws2.send('C5_Like') })
    document.getElementById("C5_Details_1").addEventListener("click", function () { ws2.send('C5_Details_1') })
    document.getElementById("C5_Details_2").addEventListener("click", function () { ws2.send('C5_Details_2') })
    document.getElementById("C5_Details_3").addEventListener("click", function () { ws2.send('C5_Details_3') })
    document.getElementById("C5_Bewerbung").addEventListener("click", function () { ws2.send('C5_Bewerbung') })

    document.getElementById("C6_URL").addEventListener("click", function () { ws2.send('C6_URL') })
    document.getElementById("C6_Like").addEventListener("click", function () { ws2.send('C6_Like') })
    document.getElementById("C6_Details_1").addEventListener("click", function () { ws2.send('C6_Details_1') })
    document.getElementById("C6_Details_2").addEventListener("click", function () { ws2.send('C6_Details_2') })
    document.getElementById("C6_Details_3").addEventListener("click", function () { ws2.send('C6_Details_3') })
    document.getElementById("C6_Bewerbung").addEventListener("click", function () { ws2.send('C6_Bewerbung') })

    document.getElementById("C7_URL").addEventListener("click", function () { ws2.send('C7_URL') })
    document.getElementById("C7_Like").addEventListener("click", function () { ws2.send('C7_Like') })
    document.getElementById("C7_Details_1").addEventListener("click", function () { ws2.send('C7_Details_1') })
    document.getElementById("C7_Details_2").addEventListener("click", function () { ws2.send('C7_Details_2') })
    document.getElementById("C7_Details_3").addEventListener("click", function () { ws2.send('C7_Details_3') })
    document.getElementById("C7_Bewerbung").addEventListener("click", function () { ws2.send('C7_Bewerbung') })

    document.getElementById("C8_URL").addEventListener("click", function () { ws2.send('C8_URL') })
    document.getElementById("C8_Like").addEventListener("click", function () { ws2.send('C8_Like') })
    document.getElementById("C8_Details_1").addEventListener("click", function () { ws2.send('C8_Details_1') })
    document.getElementById("C8_Details_2").addEventListener("click", function () { ws2.send('C8_Details_2') })
    document.getElementById("C8_Details_3").addEventListener("click", function () { ws2.send('C8_Details_3') })
    document.getElementById("C8_Bewerbung").addEventListener("click", function () { ws2.send('C8_Bewerbung') })

    document.getElementById("C9_URL").addEventListener("click", function () { ws2.send('C9_URL') })
    document.getElementById("C9_Like").addEventListener("click", function () { ws2.send('C9_Like') })
    document.getElementById("C9_Details_1").addEventListener("click", function () { ws2.send('C9_Details_1') })
    document.getElementById("C9_Details_2").addEventListener("click", function () { ws2.send('C9_Details_2') })
    document.getElementById("C9_Details_3").addEventListener("click", function () { ws2.send('C9_Details_3') })
    document.getElementById("C9_Bewerbung").addEventListener("click", function () { ws2.send('C9_Bewerbung') })

    document.getElementById("C10_URL").addEventListener("click", function () { ws2.send('C10_URL') })
    document.getElementById("C10_Like").addEventListener("click", function () { ws2.send('C10_Like') })
    document.getElementById("C10_Details_1").addEventListener("click", function () { ws2.send('C10_Details_1') })
    document.getElementById("C10_Details_2").addEventListener("click", function () { ws2.send('C10_Details_2') })
    document.getElementById("C10_Details_3").addEventListener("click", function () { ws2.send('C10_Details_3') })
    document.getElementById("C10_Bewerbung").addEventListener("click", function () { ws2.send('C10_Bewerbung') })

    document.getElementById("C11_URL").addEventListener("click", function () { ws2.send('C11_URL') })
    document.getElementById("C11_Like").addEventListener("click", function () { ws2.send('C11_Like') })
    document.getElementById("C11_Details_1").addEventListener("click", function () { ws2.send('C11_Details_1') })
    document.getElementById("C11_Details_2").addEventListener("click", function () { ws2.send('C11_Details_2') })
    document.getElementById("C11_Details_3").addEventListener("click", function () { ws2.send('C11_Details_3') })
    document.getElementById("C11_Bewerbung").addEventListener("click", function () { ws2.send('C11_Bewerbung') })

    document.getElementById("C12_URL").addEventListener("click", function () { ws2.send('C12_URL') })
    document.getElementById("C12_Like").addEventListener("click", function () { ws2.send('C12_Like') })
    document.getElementById("C12_Details_1").addEventListener("click", function () { ws2.send('C12_Details_1') })
    document.getElementById("C12_Details_2").addEventListener("click", function () { ws2.send('C12_Details_2') })
    document.getElementById("C12_Details_3").addEventListener("click", function () { ws2.send('C12_Details_3') })
    document.getElementById("C12_Bewerbung").addEventListener("click", function () { ws2.send('C12_Bewerbung') })

    document.getElementById("C13_URL").addEventListener("click", function () { ws2.send('C13_URL') })
    document.getElementById("C13_Like").addEventListener("click", function () { ws2.send('C13_Like') })
    document.getElementById("C13_Details_1").addEventListener("click", function () { ws2.send('C13_Details_1') })
    document.getElementById("C13_Details_2").addEventListener("click", function () { ws2.send('C13_Details_2') })
    document.getElementById("C13_Details_3").addEventListener("click", function () { ws2.send('C13_Details_3') })
    document.getElementById("C13_Bewerbung").addEventListener("click", function () { ws2.send('C13_Bewerbung') })

    document.getElementById("C14_URL").addEventListener("click", function () { ws2.send('C14_URL') })
    document.getElementById("C14_Like").addEventListener("click", function () { ws2.send('C14_Like') })
    document.getElementById("C14_Details_1").addEventListener("click", function () { ws2.send('C14_Details_1') })
    document.getElementById("C14_Details_2").addEventListener("click", function () { ws2.send('C14_Details_2') })
    document.getElementById("C14_Details_3").addEventListener("click", function () { ws2.send('C14_Details_3') })
    document.getElementById("C14_Bewerbung").addEventListener("click", function () { ws2.send('C14_Bewerbung') })

    document.getElementById("C15_URL").addEventListener("click", function () { ws2.send('C15_URL') })
    document.getElementById("C15_Like").addEventListener("click", function () { ws2.send('C15_Like') })
    document.getElementById("C15_Details_1").addEventListener("click", function () { ws2.send('C15_Details_1') })
    document.getElementById("C15_Details_2").addEventListener("click", function () { ws2.send('C15_Details_2') })
    document.getElementById("C15_Details_3").addEventListener("click", function () { ws2.send('C15_Details_3') })
    document.getElementById("C15_Bewerbung").addEventListener("click", function () { ws2.send('C15_Bewerbung') })

    document.getElementById("C16_URL").addEventListener("click", function () { ws2.send('C16_URL') })
    document.getElementById("C16_Like").addEventListener("click", function () { ws2.send('C16_Like') })
    document.getElementById("C16_Details_1").addEventListener("click", function () { ws2.send('C16_Details_1') })
    document.getElementById("C16_Details_2").addEventListener("click", function () { ws2.send('C16_Details_2') })
    document.getElementById("C16_Details_3").addEventListener("click", function () { ws2.send('C16_Details_3') })
    document.getElementById("C16_Bewerbung").addEventListener("click", function () { ws2.send('C16_Bewerbung') })

    document.getElementById("C17_URL").addEventListener("click", function () { ws2.send('C17_URL') })
    document.getElementById("C17_Like").addEventListener("click", function () { ws2.send('C17_Like') })
    document.getElementById("C17_Details_1").addEventListener("click", function () { ws2.send('C17_Details_1') })
    document.getElementById("C17_Details_2").addEventListener("click", function () { ws2.send('C17_Details_2') })
    document.getElementById("C17_Details_3").addEventListener("click", function () { ws2.send('C17_Details_3') })
    document.getElementById("C17_Bewerbung").addEventListener("click", function () { ws2.send('C17_Bewerbung') })

    document.getElementById("C18_URL").addEventListener("click", function () { ws2.send('C18_URL') })
    document.getElementById("C18_Like").addEventListener("click", function () { ws2.send('C18_Like') })
    document.getElementById("C18_Details_1").addEventListener("click", function () { ws2.send('C18_Details_1') })
    document.getElementById("C18_Details_2").addEventListener("click", function () { ws2.send('C18_Details_2') })
    document.getElementById("C18_Details_3").addEventListener("click", function () { ws2.send('C18_Details_3') })
    document.getElementById("C18_Bewerbung").addEventListener("click", function () { ws2.send('C18_Bewerbung') })

    document.getElementById("C19_URL").addEventListener("click", function () { ws2.send('C19_URL') })
    document.getElementById("C19_Like").addEventListener("click", function () { ws2.send('C19_Like') })
    document.getElementById("C19_Details_1").addEventListener("click", function () { ws2.send('C19_Details_1') })
    document.getElementById("C19_Details_2").addEventListener("click", function () { ws2.send('C19_Details_2') })
    document.getElementById("C19_Details_3").addEventListener("click", function () { ws2.send('C19_Details_3') })
    document.getElementById("C19_Bewerbung").addEventListener("click", function () { ws2.send('C19_Bewerbung') })

    document.getElementById("C20_URL").addEventListener("click", function () { ws2.send('C20_URL') })
    document.getElementById("C20_Like").addEventListener("click", function () { ws2.send('C20_Like') })
    document.getElementById("C20_Details_1").addEventListener("click", function () { ws2.send('C20_Details_1') })
    document.getElementById("C20_Details_2").addEventListener("click", function () { ws2.send('C20_Details_2') })
    document.getElementById("C20_Details_3").addEventListener("click", function () { ws2.send('C20_Details_3') })
    document.getElementById("C20_Bewerbung").addEventListener("click", function () { ws2.send('C20_Bewerbung') })
  };


}
webSocketInvoke();



//Funktion, um die Score-Anzeige ein- und auszublenden
function showScore() {
  var q = document.getElementById("score_area");
  if (q.style.display === "none") {
    q.style.display = "block";
  } else {
    q.style.display = "none";
  }
};

//Fixiert das Log des Scores auf dem Bildschirm
function fix_Log() {
  var w = document.getElementById("fix_log");
  if (w.style.position === "fixed") {
    w.style.position = "";
  } else {
    w.style.position = "fixed";
  }


}

//Details von Job Nr. 01
function showJob01() {
  var v = document.getElementById("show_job_01");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 02
function showJob02() {
  var v = document.getElementById("show_job_02");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};


//Details von Job Nr. 03
function showJob03() {
  var v = document.getElementById("show_job_03");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};


//Details von Job Nr. 04
function showJob04() {
  var v = document.getElementById("show_job_04");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 05
function showJob05() {
  var v = document.getElementById("show_job_05");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 06
function showJob06() {
  var v = document.getElementById("show_job_06");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 07
function showJob07() {
  var v = document.getElementById("show_job_07");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 08
function showJob08() {
  var v = document.getElementById("show_job_08");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 09
function showJob09() {
  var v = document.getElementById("show_job_09");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 10
function showJob10() {
  var v = document.getElementById("show_job_10");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 11
function showJob11() {
  var v = document.getElementById("show_job_11");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 12
function showJob12() {
  var v = document.getElementById("show_job_12");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 13
function showJob13() {
  var v = document.getElementById("show_job_13");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 14
function showJob14() {
  var v = document.getElementById("show_job_14");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 15
function showJob15() {
  var v = document.getElementById("show_job_15");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 16
function showJob16() {
  var v = document.getElementById("show_job_16");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 17
function showJob17() {
  var v = document.getElementById("show_job_17");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 18
function showJob18() {
  var v = document.getElementById("show_job_18");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 19
function showJob19() {
  var v = document.getElementById("show_job_19");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};

//Details von Job Nr. 20
function showJob20() {
  var v = document.getElementById("show_job_20");
  if (v.style.display === "none") {
    v.style.display = "block";
  } else {
    v.style.display = "none";
  }
};
